import numpy as np
from ex1 import impurity

credit_data = np.genfromtxt('credit.txt', delimiter=',', skip_header=True)

def bestsplit(x,y):
    # x: numeric input attribute (age or income or ...)
    # y: class labels, y[i] = 1 or 0
    baseImp = impurity(y)
    print("Gini-impurity of {}: {}".format(y, impurity(y)))

    # determine the split values c to check x against
    xSorted = np.sort(np.unique(x))
    print("xSorted = {}".format(xSorted))
    splits = (xSorted[0:len(xSorted)-1]+xSorted[1:len(xSorted)])/2
    print("splits = {}".format(splits))

    bestSplit = len(credit_data)+1 # placeholder, for out of bounds
    bestReduction = 0 # placeholder, no improvement

    # check each split for its impurity reduction
    for c in splits: # brute force approach
        splitImp = impurity(y[x > c])
        print("Split on {} reduces impurity by {}".format(c, baseImp - splitImp))
        if (baseImp - splitImp > bestReduction):
            bestReduction = baseImp - splitImp
            bestSplit = c

    return bestSplit

# print("Best split = {}".format(bestsplit(credit_data[:,3],credit_data[:,5]))) # 36