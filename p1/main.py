# UU Data Mining assignment 1 by
# Martijn Goes (6299474)
# Glenn Hamers (6431763)
# Lisanne Koetsier (6522289)

import numpy as np
import random
import sys

def tree_grow(x, y, nmin, minleaf, nfeat):
    """
    Returns a decision tree on which new (unseen) samples can be processed in order to classify them.

    x (2D int array): attribute-values (one row = one sample)
    y (int array): class labels (0 or 1), assume no missing values
    nmin (int): minimum number of samples required for a node to split
    minleaf (int): minimum number of samples required in a leaf
    nfeat (int): number of features to consider for a split
    returns a tree object
    """
    sample_count = np.shape(x)[0]
    y = np.array(y)
    base_impurity = gini_impurity(y)

    splits = []
    leafs = []
    skip_list = []
    recursive_split(x, y, 0, False, skip_list, splits,
                    leafs, nmin, minleaf, nfeat, sample_count, base_impurity)
    return build_tree(splits, leafs)


def tree_pred(x, tr):
    """
    Given a set of unseen cases, process them in the decision tree to label them as 0 or 1.

    x (2D int array): numerical attribute values of the cases for which predictions are required (i.e. samples)
    tr (DecisionTree): decision tree object created with tree_grow
    returns y, a list with all the predictions (either 0 or 1)
    """

    y = []
    node = tr.root_node
    x = x[0]
    for sample in x:  # check every sample
        while not node.isLeaf:  # go through the tree untill a leaf is reached
            col = node.column
            val = node.value
            if sample[col] <= val:
                node = node.leftChild
            else:
                node = node.rightChild

        if node.isLeaf:
            y.append(node.leafMajority)  # add result of the sample to outcome
            node = tr.root_node  # reset the function to the root node

    return y


def tree_grow_b(x, y, nmin, minleaf, nfeat, m):
    """
    Comparable to tree_grow, but now use bootstrap samples.

    x (2D int array): attribute-values (one row = one sample)
    y (int array): class labels (0 or 1), assume no missing values
    nmin (int): minimum number of samples required for a node to split
    minleaf (int): minimum number of samples required in a leaf
    nfeat (int): number of features to consider for a split
    m (int): number of bootstrap samples to be drawn

    returns a list of trees
    """

    splits = []
    leafs = []
    skip_list = []
    trees = []
    
    sample_count = np.shape(x)[0]
    y = np.array(y)
    base_impurity = gini_impurity(y)

    original_x = x
    original_y = y
    for _ in range(0, m):
        # draw random sample IDs, with replacement
        random_row_ids = np.random.randint(np.shape(original_x)[0], size=np.shape(original_x)[0])
        x = original_x[random_row_ids, :]
        y = original_y[random_row_ids]
        if np.shape(x)[1] == nfeat:
            rf_bool = False
        else:
            rf_bool = True
        recursive_split(x, y, 0, rf_bool, skip_list, splits, leafs,
                        nmin, minleaf, nfeat, sample_count, base_impurity)
        trees.append(build_tree(splits, leafs))
    return trees


def tree_pred_b(x, tr):
    """
    Comparable to tree_pred, but now check all trees built by tree_grow_b.

    x (2D int array): numerical attribute values of the cases for which predictions are required (i.e. samples)
    tr (DecisionTree): decision tree object created with tree_grow
    returns y, a list with all the predictions (either 0 or 1)
    """

    y = []
    one_sample_predictions = []  # holds the predictions for one sample given by all trees

    x = x[0]
    for sample in x:  # check every sample
        for tree in tr:  # check every tree
            node = tree.root_node
            while not node.isLeaf:  # go through the tree until a leaf is hit
                col = node.column
                val = node.value
                if sample[col] <= val:
                    node = node.leftChild
                else:
                    node = node.rightChild

            if node.isLeaf:
                # add result of the sample to outcome
                one_sample_predictions.append(node.leafMajority)
                node = tree.root_node  # reset the function to the root node
        one_sample_predictions = np.array(one_sample_predictions)
        ones = len(one_sample_predictions[one_sample_predictions == 1])
        zeros = len(one_sample_predictions[one_sample_predictions == 0])
        if ones > zeros:
            # sample was classified as 1 more times than it was classified as 0
            y.append(1)
        else:
            y.append(0)
        one_sample_predictions = []  # reset for next sample
    return y


class DecisionTree:
    """
    A class used to represent the decision tree.
    This decision tree will be used to predict the class label of a new case.
    """

    def __init__(self, root_node):
        """
        root_node (DecisionNode): the first element at the root of the tree
        """
        self.root_node = root_node


class DecisionNode:
    """
    A class used to represent an element of the decision tree.
    A node will either be:
    -- a split node (tuple (int, int, int)): represents (node id, attribute column, split value)
    -- a leaf node (tuple (int, int, int)): represents (node id, number of 0-class assignments, numer of 1-class assignments)
    """

    def __init__(self, id, column, value, parent_id):
        """
        id (int): unique node indentifier
        column (int): attribute column ID to split on
        value (float): split value/threshold (usually denoted as 'c')
        parent_id (int): ID needed to identify child-parent relations
        """
        self.id = id
        self.column = column
        self.value = value
        self.isLeaf = False # initially, a node is a split node, which can later be turned into a leaf node (once the algorithm decides to)
        self.leftChild = None # children will be added by the algorithm after creating the node
        self.rightChild = None
        self.parent_id = parent_id
        self.leafMajority = None  # majority class, to be determined once the split node is converted to a leaf node


def recursive_split(x, y, id, rf_bool, skip_list, splits, leafs, nmin, minleaf, nfeat, sample_count, base_impurity):
    """
    Recursively split the data whilst finding the optimal splits. 
    It writes them to the "splits" and "leafs" lists,
    which later will be turned into a complete DecisionTree (where tree_pred works on).

    x (2D int array): attribute-values (one row = one sample)
    y (int array): class labels (0 or 1), assume no missing values
    id (int): node ID in the decision tree
    rf_bool (boolean): True = random forest activated, False = normal tree growing
    skip_list (int list): columns that have to be skipped, because they violate the minleaf constraint
    splits (int list): the currently known split values (nodes)
    leafs (int list): the currently known leaf values
    nmin (int): minimum number of samples required for a node to split
    minleaf (int): minimum number of samples required in a leaf
    nfeat (int): number of features to consider for a split
    sample_count: the number of samples in x
    base_impurity: the gini impurity of the root node 
    """

    if len(skip_list) == 0:  # no need to increase the node id if we look for another (best possible) split as the previous one was not possible
        id = id + 1          # meaning, we are still processing the same node
    if len(x) < nmin:  # too small to split any further, so make it a leaf
        leafs.append((id, len(y[y == 0]), len(y[y == 1])))
        return id
    else:
        if len(np.unique(y)) == 1:
            # every item has the same class label: no reason to split any further, add leaf
            leafs.append((id, len(y[y == 0]), len(y[y == 1])))
            return id

        # temporary variables to store best found splits during the loop
        best_split_value = -1
        best_impurity_reduction = 0
        best_column = -1

        features = []
        features_chosen = np.arange(len(x[0]))
        if rf_bool:  # take at random nfeat featurees
            for _ in range(0, nfeat):
                random_feature_index = random.randint(
                    0, len(features_chosen) - 1)
                features.append(features_chosen[random_feature_index])
                features_chosen = np.delete(
                    features_chosen, random_feature_index)
        else:
            features = features_chosen

        for i in features:
            if i in skip_list:
                continue
            # try to split, without doing the split
            test_results = test_split(i, x, y, id, sample_count, base_impurity)
            impurity_reduction = test_results[1]
            if impurity_reduction != sys.maxsize:  # removes check if all items have the same classification in the column
                if impurity_reduction > best_impurity_reduction:
                    best_column = i
                    best_split_value = test_results[0]
                    best_impurity_reduction = impurity_reduction
        if best_split_value == -1: # no (allowed) split was found, so make a leaf of this node
            leafs.append((id, len(y[y == 0]), len(y[y == 1])))
            return id

        # make the actual split
        xFeature = x[:, best_column]
        leftX = x[xFeature <= best_split_value]
        rightX = x[xFeature > best_split_value]
        leftY = y[xFeature <= best_split_value]
        rightY = y[xFeature > best_split_value]

        if (len(leftX) < minleaf or len(rightX) < minleaf):
            # best split violates minleaf constraint, so look for another split on the same level
            skip_list.append(best_column)
            id = recursive_split(
                x, y, id, rf_bool, skip_list, splits, leafs, nmin, minleaf, nfeat, sample_count, base_impurity)
        else:
            splits.append((id, best_column, best_split_value))
            id = recursive_split(leftX, leftY, id, rf_bool, [
            ], splits, leafs, nmin, minleaf, nfeat, sample_count, base_impurity)  # recurse through the left side
            id = recursive_split(rightX, rightY, id, rf_bool, [
            ], splits, leafs, nmin, minleaf, nfeat, sample_count, base_impurity)  # and right side
        return id


def best_split(x, y, split_values, column, sample_count, base_impurity):
    """
    Helper function of test_split-function.
    Returns the column index that has the highest impurity reduction + the reduction value itself.

    x (2D int array): attribute-values (one row = one sample)
    y (int array): class labels (0 or 1), assume no missing values
    split_values (int list): c values, i.e. x <= c
    column (int): attribute column index
    sample_count: the number of samples in x
    base_impurity: the gini impurity of the root node 
    """

    best_split_value = None
    best_reduction = 0
    for splitval in split_values:  # check all possible split values
        idx_left = []
        idx_right = []
        # check for all items if it belongs to the left or right side
        for count, xlist in enumerate(x):
            if xlist[column] <= splitval:
                idx_left.append(count)
            else:
                idx_right.append(count)
        left = [y[i] for i in idx_left]
        right = [y[i] for i in idx_right]
        left = np.array(left)
        right = np.array(right)

        impurity_left = gini_impurity(left)
        reduction_left = impurity_left * len(left) / sample_count
        impurity_right = gini_impurity(right)
        reduction_right = impurity_right * len(right) / sample_count
        total_reduction = base_impurity - reduction_left - reduction_right
        # get the best column, which results in the best impurity reduction value
        if (total_reduction > best_reduction):
            best_reduction = total_reduction
            best_split_value = splitval
    return (best_split_value, best_reduction)


def test_split(column, x, y, id, sample_count, base_impurity):
    """
    Find the value of column that has the best split impurity reduction.

    column (int): attribute column index
    x (2D int array): attribute-values (one row = one sample)
    y (int array): class labels (0 or 1), assume no missing values
    id (int): node id, used to return sys.maxsize to indicate that this node has identical class labels for all cases
    sample_count: the number of samples in x
    base_impurity: the gini impurity of the root node 

    If no split has been found it returns a tuple of an id and sys.max 
    If a split has been found it returns the float of the split value
    """
    xFeature = x[:, column]  # the column with the correct attribute to split on
    xSorted = np.sort(np.unique(xFeature))
    if len(xSorted) < 2:  # all nodes in this split belong to the same class, so no split can be found
        return (id, sys.maxsize)
    else:
        # (just like practice exercise 2) determine the splits, so the average of the values of i and i+1
        split_values = (xSorted[0:len(xSorted)-1]+xSorted[1:len(xSorted)])/2
        best_split_value = best_split(x, y, split_values, column, sample_count, base_impurity)
        return best_split_value


def gini_impurity(a):
    """
    Calculate the gini impurity
    
    a (binary list): list of binary values on which to compute the gini impurity
    returns a float value 
    """
    # Gini OF A NODE (1D array, a) = class0 * class1 / nodeSize; Note that this is different from the impurity REDUCTION function.
    nodeSize = len(a)
    ones = 0
    zeroes = 0
    for i in range(0,nodeSize):
        if a[i] == 1:
            ones = ones + 1
        else:
            zeroes = zeroes +1
    return zeroes / nodeSize * ones / nodeSize


def build_tree(nodes, leafs):
    """
    Given nodes/leafs and their IDs, reconstruct the decision tree.

    nodes (tuple (int, int, int) list): tuples of (id, column of x, c), hold a rule to split on, i.e. x[i] <= c
    leafs (tuple (int, int, int) list): tuples of (id, class 0 count, class 1 count), hold the class distribution

    returns a decision tree
    """

    if len(nodes) > 0:  # there is an actual tree with nodes, not just a single leaf
        node = nodes.pop(0)
        leaf = leafs.pop(0)
        # creates the rootnode of the decision tree ...
        parentNode = DecisionNode(node[0], node[1], node[2], -1)
        # ... and creates the decision tree
        decisiontree = DecisionTree(parentNode)
        if len(nodes) > 0:
            node = nodes.pop(0)
        else:
            node = []
        # recurse left side from root node, return values as we need them when we go into the right side
        tuple = decision_recursion(node, leaf, nodes, leafs, parentNode, True)
        # recurse right side from root node
        decision_recursion(tuple[2], tuple[0], tuple[3],
                           tuple[1], parentNode, False)
    else:  # tree consists only of a singular node (so a leaf)
        leaf = leafs.pop(0)
        parentNode = DecisionNode(leaf[0], leaf[1], leaf[2], -1)
        parentNode.isLeaf = True
        decisiontree = DecisionTree(parentNode)
        if leaf[1] > leaf[2]:
            parentNode.leafMajority = 0
        else:
            parentNode.leafMajority = 1
    return decisiontree


def decision_recursion(node, leaf, nodes, leafs, parent, left):
    """
    Recursively build the decision tree given sets of nodes and leafs.

    node (tuple(int, int, int)): the node next to process
    leaf (tuple(int, int, int)): the leaf next to process
    nodes (tuple(int, int, int) list): queue of nodes to process
    leafs (tuple(int, int, int) list): queue of leafs to process
    parent (DecisionNode): the parent node of the node/leaf being processed
    left (boolean): indicating if we are working with a left child or right child
    """
    if node != [] and node[0] < leaf[0]:  # next addition is a node to the tree
        # add node to the tree using the parent of this new node
        childNode = DecisionNode(node[0], node[1], node[2], parent.id)
        if left:
            parent.leftChild = childNode
        else:
            parent.rightChild = childNode

        if len(nodes) != 0:  # if there are nodes left in the queue to be added
            node = nodes.pop(0)
        else:
            node = []
        tuple = decision_recursion(node, leaf, nodes, leafs, childNode, True)
        tuple = decision_recursion(
            tuple[2], tuple[0], tuple[3], tuple[1], childNode, False)
        return tuple
    elif leaf != []:  # next addition is a leaf to the tree
        childNode = DecisionNode(leaf[0], leaf[1], leaf[2], parent.id)
        childNode.isLeaf = True
        if leaf[1] > leaf[2]:
            childNode.leafMajority = 0
        else:
            childNode.leafMajority = 1

        if left:
            parent.leftChild = childNode
        else:
            parent.rightChild = childNode

        if len(leafs) != 0:
            leaf = leafs.pop(0)
        else:
            leaf = []
        return (leaf, leafs, node, nodes)
    else: # no nodes or leafs left to add, so return
        return


def readcsv_eclipse(eclipse):
    """
    Read the provided csv file and process it into the correct format.
    This means extracting the classifier column and adapting it to be 0 or 1.
    And create the attribute data consisting of column 2 to 44, excluding column 3 (the classifier column) 

    eclipse (string): the file to read
    returns a tuple of an array with the predictor variables (csv_eclipse) and an array of the classifier
    """
    csv_eclipse = np.genfromtxt(eclipse, delimiter=';', skip_header=True)
    classifier = csv_eclipse[:, 3]  # get the classifier numbers
    # make the classifier numbers binary
    classifier = [1 if x >= 1 else 0 for x in classifier]
    csv_eclipse = csv_eclipse[:, 2:44]  # take the wanted attributes
    csv_eclipse = np.delete(csv_eclipse, 1, 1)  # remove the classifier column
    return (csv_eclipse, classifier)