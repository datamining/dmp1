import numpy as np

array=np.array([1,0,1,1,1,0,0,1,1,0,1]) # imp = 0.23140495867768596

def impurity(arr):
    sum = len(arr)
    ones = len(arr[arr[:] > 0])
    zeroes = sum - ones
    imp = ones/sum * zeroes/sum
    return imp

print("Gini-impurity of {}: {}".format(array, impurity(array)))